# Classical and quantum random-walk centrality measures in multilayer networks

This project provides simulation frameworks for continuous-time classical and quantum random-walk centralities on arbitrary (multilayer) networks.

The ``walkerlib`` directory contains definitions of classical and quantum Hamiltonians, analytical solutions of the occupation probability distributions, and numerical integration routines to simulate the evolution of classical and quantum on arbitrary (multilayer) networks. Based on these routines, one can calculate classical and quantum versions of occupation, PageRank, betweenness, and closeness centralities.

You can run the examples ``qc_rwoc_ERER.py``, ``qc_rwoc_ERBA.py``, ``qc_pagerank_ERER.py``, and ``qc_pagerank_ERBA.py``. After plotting the results using ``qc_rwoc_ERER_ERBA.py``, you should be able to obtain the following plot: 

<p align="center">
  <img src="oc_synthetic.png">
</p>

Orange crosses and blue disks are classical and quantum centralities, respectively. 

Our implementations use the [parmap](https://github.com/zeehio/parmap) python module to parallelize (classical and quantum random-walk) betweenness/closeness centrality calculations.

## Reference
* L. Böttcher, M. A. Porter, Classical and quantum random-walk centrality measures in multilayer networks, SIAM Journal on Applied Mathematics 81, 2704--2724 (2021)

```
@article{boettcher2021classical,
  title={Classical and quantum random-walk centrality measures in multilayer networks},
  author={B\"ottcher, Lucas and Porter, Mason A},
  journal={SIAM Journal on Applied Mathematics},
  volume={81},
  number={6},
  pages={2704--2724},
  year={2021},
  publisher={SIAM}
}
```
