import numpy as np
import networkx as nx
import multinetx as mx
import time
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

from walkerlib.hamiltonians import pr_CHamiltonian, pr_QHamiltonian
from walkerlib.integration import simulate_qc
from walkerlib.utilities import load_multiplex

from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 8,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 183    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

# multilayer network
M = load_multiplex('../LazegaLawyers/edges.csv')

plt.figure()
plt.imshow(mx.adjacency_matrix(M,weight='weight').todense(),
		  origin='upper',interpolation='nearest',cmap=plt.cm.jet_r)
plt.show()

rt = 0.85

# quantum Hamiltonian
HQ = pr_QHamiltonian(M, rt)
    
# classical Hamiltonian
HC = pr_CHamiltonian(M, rt)

# time step and simulation time
dt = 0.01
T = 100
N = int(T/dt)

# reset rate
r = 0

# initializing wave function
phim1Q0 = np.ones(len(M))
phim1Q0 /= np.linalg.norm(phim1Q0)    
phiQ0 = phim1Q0-HQ.dot(phim1Q0)*dt

# initializing probability distribution
phiC0 = np.ones(len(M))
phiC0 /= sum(phiC0) 

start_time = time.time()   

# simulate classical and quantum walker
degree_arr, classical_observation_prob, \
            quantum_observation_prob = simulate_qc(M, 
                                                   HC, 
                                                   HQ, 
                                                   phim1Q0, 
                                                   phiQ0, 
                                                   phiC0, 
                                                   N, 
                                                   dt, 
                                                   r, 
                                                   average_threshold = 8000)

print("--- %s seconds runtime ---" % (time.time() - start_time))

np.savetxt("Lazega_ML_RWPR.csv", 
           np.c_[degree_arr, classical_observation_prob, quantum_observation_prob], 
           delimiter = '\t', 
           header = 'geodesic betweenness \t classical betweenness \t quantum betweenness')

fig, ax = plt.subplots()
plt.plot(degree_arr, quantum_observation_prob, marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
plt.plot(degree_arr, classical_observation_prob, marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')

plt.text(0.02*50, 0.88*15, r"(c)")

ax.xaxis.set_minor_locator(MultipleLocator(5))
plt.xlim([0,50])
plt.ylim([0,15])
plt.yticks([0,5,10,15])
plt.legend(loc = 4, frameon = False, fontsize = 6)
plt.xlabel(r'degree')
plt.ylabel(r'occupation prob. ($10^{-3}$)')
plt.tight_layout()
plt.savefig(r'Lazega_ML_PR.png', dpi = 300)