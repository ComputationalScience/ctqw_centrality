import numpy as np
import scipy as sp
import networkx as nx
import multinetx as mx
import time
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

from walkerlib.integration import random_walk_betweenness

from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 10,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 2*183    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

ERER_ML_RWBC_deg = np.loadtxt('ERER_ML_RWBC_deg.csv', dtype = complex)
ERBA_ML_RWBC_deg = np.loadtxt('ERBA_ML_RWBC_deg.csv', dtype = complex)

ERER_ML_RWBC = np.loadtxt('ERER_ML_RWBC.csv', dtype = complex)
ERBA_ML_RWBC = np.loadtxt('ERBA_ML_RWBC.csv', dtype = complex)

ERER_ML_RWCC_deg = np.loadtxt('ERER_ML_RWCC_deg.csv', dtype = complex)
ERBA_ML_RWCC_deg = np.loadtxt('ERBA_ML_RWCC_deg.csv', dtype = complex)

ERER_ML_RWCC = np.loadtxt('ERER_ML_RWCC.csv', dtype = complex)
ERBA_ML_RWCC = np.loadtxt('ERBA_ML_RWCC.csv', dtype = complex)

fig, ax = plt.subplots(nrows = 2, ncols = 2)

ax[0][0].text(0.02*80+20, 0.85*6, r"(a)")
ax[0][0].plot(ERER_ML_RWBC_deg[:,0], 1e5*ERER_ML_RWBC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][0].plot(ERER_ML_RWBC_deg[:,0], 1e5*ERER_ML_RWBC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][0].xaxis.set_minor_locator(MultipleLocator(5))
ax[0][0].set_xlim([20,100])
ax[0][0].set_ylim([0,6])
ax[0][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][0].set_xlabel(r'degree')
ax[0][0].set_ylabel(r'betweenness $(10^{-5})$')

ax[0][1].text(0.02*60, 0.85*8, r"(b)")
ax[0][1].plot(ERBA_ML_RWBC_deg[:,0], 1e5*ERBA_ML_RWBC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][1].plot(ERBA_ML_RWBC_deg[:,0], 1e5*ERBA_ML_RWBC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][1].xaxis.set_minor_locator(MultipleLocator(5))
ax[0][1].set_xlim([0,60])
ax[0][1].set_xticks([0,10,20,30,40,50,60])
ax[0][1].set_ylim([0,8])
ax[0][1].set_yticks([0,4,8])
ax[0][1].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][1].set_xlabel(r'degree')
ax[0][1].set_ylabel(r'betweenness $(10^{-5})$')

ax[1][0].text(0.02*80+20, 0.85*3, r"(c)")
ax[1][0].plot(ERER_ML_RWCC_deg[:,0], ERER_ML_RWCC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][0].plot(ERER_ML_RWCC_deg[:,0], ERER_ML_RWCC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][0].xaxis.set_minor_locator(MultipleLocator(5))
ax[1][0].set_xlim([20,100])
ax[1][0].set_ylim([0,3])
ax[1][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[1][0].set_xlabel(r'degree')
ax[1][0].set_ylabel(r'closeness')

ax[1][1].text(0.02*60, 0.85*8, r"(d)")
ax[1][1].plot(ERBA_ML_RWCC_deg[:,0], ERBA_ML_RWCC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][1].plot(ERBA_ML_RWCC_deg[:,0], ERBA_ML_RWCC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][1].xaxis.set_minor_locator(MultipleLocator(5))
ax[1][1].set_xlim([0,60])
ax[1][1].set_xticks([0,10,20,30,40,50,60])
ax[1][1].set_ylim([0,8])
ax[1][1].set_yticks([0,4,8])
ax[1][1].legend(loc = 1, frameon = False, fontsize = 6)
ax[1][1].set_xlabel(r'degree')
ax[1][1].set_ylabel(r'closeness')

plt.tight_layout()
plt.margins(0,0)
plt.savefig('bc_cc_synthetic.png', dpi = 480, bbox_inches = 'tight', pad_inches = 0.05)

fig, ax = plt.subplots(nrows = 2, ncols = 2)

print(pearsonr(ERER_ML_RWBC[:,0], ERER_ML_RWBC[:,2]))
print(pearsonr(ERER_ML_RWBC[:,0], ERER_ML_RWBC[:,1]))

ax[0][0].text(0.02*2, 0.85*6, r"(a)")
ax[0][0].plot(ERER_ML_RWBC[:,0]*1e3, ERER_ML_RWBC[:,2]*1e5, marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][0].plot(ERER_ML_RWBC[:,0]*1e3, ERER_ML_RWBC[:,1]*1e5, marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][0].xaxis.set_minor_locator(MultipleLocator(0.1))
ax[0][0].set_xlim([0,2])
ax[0][0].set_ylim([0,6])
ax[0][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][0].set_xlabel(r'geodesic betweenness')
ax[0][0].set_ylabel(r'rw~betweenness $(10^{-5})$')

print(pearsonr(ERBA_ML_RWBC[:,0], ERBA_ML_RWBC[:,2]))
print(pearsonr(ERBA_ML_RWBC[:,0], ERBA_ML_RWBC[:,1]))

ax[0][1].text(0.02*20, 0.85*15, r"(b)")
ax[0][1].plot(ERBA_ML_RWBC[:,0]*1e3, ERBA_ML_RWBC[:,2]*1e5, marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][1].plot(ERBA_ML_RWBC[:,0]*1e3, ERBA_ML_RWBC[:,1]*1e5, marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][1].xaxis.set_minor_locator(MultipleLocator(1))
ax[0][1].set_xlim([0,20])
ax[0][1].set_ylim([0,15])
ax[0][1].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][1].set_xlabel(r'geodesic betweenness')
ax[0][1].set_ylabel(r'rw~betweenness $(10^{-5})$')


print(pearsonr(ERER_ML_RWCC[:,0], ERER_ML_RWCC[:,2]))
print(pearsonr(ERER_ML_RWCC[:,0], ERER_ML_RWCC[:,1]))

ax[1][0].text(0.02*0.1+0.35, 0.85*2, r"(c)")
ax[1][0].plot(ERER_ML_RWCC[:,0], ERER_ML_RWCC[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][0].plot(ERER_ML_RWCC[:,0], ERER_ML_RWCC[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][0].xaxis.set_minor_locator(MultipleLocator(0.005))
ax[1][0].set_xlim([0.35,0.45])
ax[1][0].set_ylim([0,2])
ax[1][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[1][0].set_xlabel(r'geodesic closeness')
ax[1][0].set_ylabel(r'rw~closeness')

print(pearsonr(ERBA_ML_RWCC[:,0], ERBA_ML_RWCC[:,2]))
print(pearsonr(ERBA_ML_RWCC[:,0], ERBA_ML_RWCC[:,1]))

ax[1][1].text(0.02*0.3+0.2, 0.85*4, r"(d)")
ax[1][1].plot(ERBA_ML_RWCC[:,0], ERBA_ML_RWCC[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][1].plot(ERBA_ML_RWCC[:,0], ERBA_ML_RWCC[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][1].xaxis.set_minor_locator(MultipleLocator(0.02))
ax[1][1].set_xlim([0.2,0.5])
ax[1][1].set_ylim([0,4])
ax[1][1].legend(loc = 1, frameon = False, fontsize = 6)
ax[1][1].set_xlabel(r'geodesic closeness')
ax[1][1].set_ylabel(r'rw~closeness')

plt.tight_layout()
plt.margins(0,0)
plt.savefig('bc_cc_synthetic_corr.png', dpi = 480, bbox_inches = 'tight', pad_inches = 0.05)







