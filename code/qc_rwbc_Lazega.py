import numpy as np
import scipy as sp
import networkx as nx
import multinetx as mx
import time
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

from walkerlib.integration import random_walk_betweenness
from walkerlib.utilities import load_multiplex

from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 10,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 183    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

# multilayer network
M = load_multiplex('../LazegaLawyers/edges.csv')

plt.figure()
plt.imshow(mx.adjacency_matrix(M,weight='weight').todense(),
		  origin='upper',interpolation='nearest',cmap=plt.cm.jet_r)
plt.show()

s = 0.01
classical_betweenness, quantum_betweenness, degree_arr, classical_betweenness_deg, \
            quantum_betweenness_deg = random_walk_betweenness(M, s, procs = 2)

geo_betweenness = [x for x in nx.betweenness_centrality(M).values()]

np.savetxt("Lazega_ML_RWBC.csv", np.c_[geo_betweenness, classical_betweenness, quantum_betweenness], delimiter = '\t', header = 'geodesic betweenness \t classical betweenness \t quantum betweenness')
np.savetxt("Lazega_ML_RWBC_deg.csv", np.c_[degree_arr, classical_betweenness_deg, \
            quantum_betweenness_deg], delimiter = '\t', header = 'degree \t classical betweenness \t quantum betweenness')

fig, ax = plt.subplots()
plt.plot(degree_arr, quantum_betweenness_deg, marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
plt.plot(degree_arr, classical_betweenness_deg, marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')

ax.xaxis.set_minor_locator(MultipleLocator(5))
#plt.xlim([20,100])
#plt.ylim([0,100])
plt.legend(loc = 4, frameon = False, fontsize = 6)
plt.xlabel(r'degree')
plt.ylabel(r'betweenness')
plt.tight_layout()
plt.savefig(r'Lazega_ML_RWBC.png', dpi = 300)

      
plt.figure()
plt.plot(geo_betweenness, quantum_betweenness, 'o', markersize = 4, label = r'quantum')
plt.plot(geo_betweenness, classical_betweenness, 'x', markersize = 4, label = r'classical')
plt.xlabel(r'geodesic betweenness')
plt.ylabel(r'random-walk betweenness')
plt.legend(loc = 4, frameon = False, fontsize = 6)
plt.tight_layout()
plt.savefig(r'Lazega_ML_RWBC_corr.png', dpi = 300)
plt.show()
