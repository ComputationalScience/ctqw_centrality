import numpy as np
import scipy as sp
import networkx as nx
import multinetx as mx
import time
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

from walkerlib.integration import random_walk_betweenness

from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 10,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 2*183    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

London_ML_RWBC_deg = np.loadtxt('London_ML_RWBC_deg.csv', dtype = complex)
Lazega_ML_RWBC_deg = np.loadtxt('Lazega_ML_RWBC_deg.csv', dtype = complex)

London_ML_RWBC = np.loadtxt('London_ML_RWBC.csv', dtype = complex)
Lazega_ML_RWBC = np.loadtxt('Lazega_ML_RWBC.csv', dtype = complex)

London_ML_RWCC_deg = np.loadtxt('London_ML_RWCC_deg.csv', dtype = complex)
Lazega_ML_RWCC_deg = np.loadtxt('Lazega_ML_RWCC_deg.csv', dtype = complex)

London_ML_RWCC = np.loadtxt('London_ML_RWCC.csv', dtype = complex)
Lazega_ML_RWCC = np.loadtxt('Lazega_ML_RWCC.csv', dtype = complex)

fig, ax = plt.subplots(nrows = 2, ncols = 2)

ax[0][1].text(0.02*10, 0.85*30, r"(b)")
ax[0][1].plot(London_ML_RWBC_deg[:,0], 1e5*London_ML_RWBC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][1].plot(London_ML_RWBC_deg[:,0], 1e5*London_ML_RWBC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][1].xaxis.set_minor_locator(MultipleLocator(1))
ax[0][1].set_xlim([0,10])
ax[0][1].set_ylim([0,30])
ax[0][1].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][1].set_xlabel(r'degree')
ax[0][1].set_ylabel(r'betweenness $(10^{-5})$')

ax[0][0].text(0.02*50, 0.85*600, r"(a)")
ax[0][0].plot(Lazega_ML_RWBC_deg[:,0], 1e5*Lazega_ML_RWBC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][0].plot(Lazega_ML_RWBC_deg[:,0], 1e5*Lazega_ML_RWBC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][0].xaxis.set_minor_locator(MultipleLocator(5))
ax[0][0].set_xlim([0,50])
ax[0][0].set_ylim([0,600])
ax[0][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][0].set_xlabel(r'degree')
ax[0][0].set_ylabel(r'betweenness $(10^{-5})$')

ax[1][1].text(0.02*10, 0.85*4, r"(d)")
ax[1][1].plot(London_ML_RWCC_deg[:,0], London_ML_RWCC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][1].plot(London_ML_RWCC_deg[:,0], London_ML_RWCC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][1].xaxis.set_minor_locator(MultipleLocator(1))
ax[1][1].set_xlim([0,10])
ax[1][1].set_ylim([0,4])
ax[1][1].legend(loc = 4, frameon = False, fontsize = 6)
ax[1][1].set_xlabel(r'degree')
ax[1][1].set_ylabel(r'closeness')

ax[1][0].text(0.02*60, 0.85*2, r"(c)")
ax[1][0].plot(Lazega_ML_RWCC_deg[:,0], Lazega_ML_RWCC_deg[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][0].plot(Lazega_ML_RWCC_deg[:,0], Lazega_ML_RWCC_deg[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][0].xaxis.set_minor_locator(MultipleLocator(5))
ax[1][0].set_xlim([0,50])
ax[1][0].set_ylim([0,2])
ax[1][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[1][0].set_xlabel(r'degree')
ax[1][0].set_ylabel(r'closeness')

plt.tight_layout()
plt.margins(0,0)
plt.savefig('bc_cc_empirical.png', dpi = 480, bbox_inches = 'tight', pad_inches = 0.05)

fig, ax = plt.subplots(nrows = 2, ncols = 2)

print(pearsonr(London_ML_RWBC[:,0], London_ML_RWBC[:,2]))
print(pearsonr(London_ML_RWBC[:,0], London_ML_RWBC[:,1]))

ax[0][1].text(0.02*300, 0.85*30, r"(b)")
ax[0][1].plot(London_ML_RWBC[:,0]*1e3, London_ML_RWBC[:,2]*1e5, marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][1].plot(London_ML_RWBC[:,0]*1e3, London_ML_RWBC[:,1]*1e5, marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][1].xaxis.set_minor_locator(MultipleLocator(20))
ax[0][1].set_xlim([0,300])
ax[0][1].set_ylim([0,30])
ax[0][1].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][1].set_xlabel(r'geodesic betweenness')
ax[0][1].set_ylabel(r'rw~betweenness $(10^{-5})$')

print(pearsonr(Lazega_ML_RWBC[:,0], Lazega_ML_RWBC[:,2]))
print(pearsonr(Lazega_ML_RWBC[:,0], Lazega_ML_RWBC[:,1]))

ax[0][0].text(0.02*30, 0.85*600, r"(a)")
ax[0][0].plot(Lazega_ML_RWBC[:,0]*1e3, Lazega_ML_RWBC[:,2]*1e5, marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][0].plot(Lazega_ML_RWBC[:,0]*1e3, Lazega_ML_RWBC[:,1]*1e5, marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[0][0].xaxis.set_minor_locator(MultipleLocator(2))
ax[0][0].set_xlim([0,30])
ax[0][0].set_ylim([0,600])
ax[0][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][0].set_xlabel(r'geodesic betweenness')
ax[0][0].set_ylabel(r'rw~betweenness $(10^{-5})$')

print(pearsonr(London_ML_RWCC[:,0], London_ML_RWCC[:,2]))
print(pearsonr(London_ML_RWCC[:,0], London_ML_RWCC[:,1]))

ax[1][1].text(0.02*0.1+0.02, 0.85*4, r"(d)")
ax[1][1].plot(London_ML_RWCC[:,0], London_ML_RWCC[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][1].plot(London_ML_RWCC[:,0], London_ML_RWCC[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][1].xaxis.set_minor_locator(MultipleLocator(0.005))
ax[1][1].set_xlim([0.02,0.12])
ax[1][1].set_ylim([0,4])
ax[1][1].legend(loc = (0.15,0.7), frameon = False, fontsize = 6)
ax[1][1].set_xlabel(r'geodesic closeness')
ax[1][1].set_ylabel(r'rw~closeness')

print(pearsonr(Lazega_ML_RWCC[:,0], Lazega_ML_RWCC[:,2]))
print(pearsonr(Lazega_ML_RWCC[:,0], Lazega_ML_RWCC[:,1]))

ax[1][0].text(0.02*0.4+0.2, 0.85*2, r"(c)")
ax[1][0].plot(Lazega_ML_RWCC[:,0], Lazega_ML_RWCC[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][0].plot(Lazega_ML_RWCC[:,0], Lazega_ML_RWCC[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')
ax[1][0].xaxis.set_minor_locator(MultipleLocator(0.02))
ax[1][0].set_xlim([0.2,0.6])
ax[1][0].set_ylim([0,2])
ax[1][0].legend(loc = (0.15,0.7), frameon = False, fontsize = 6)
ax[1][0].set_xlabel(r'geodesic closeness')
ax[1][0].set_ylabel(r'rw~closeness')

plt.tight_layout()
plt.margins(0,0)
plt.savefig('bc_cc_empirical_corr.png', dpi = 480, bbox_inches = 'tight', pad_inches = 0.05)






