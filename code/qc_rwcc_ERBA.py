import numpy as np
import scipy as sp
import networkx as nx
import multinetx as mx
import time
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

from walkerlib.integration import closeness_centrality

from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 10,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 183    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

# multilayer network
N = 1000
L1 = mx.generators.erdos_renyi_graph(N, 0.04, seed = 1235)
L2 = mx.generators.barabasi_albert_graph(N, m = 2, seed = 3123)

adj_block = mx.lil_matrix(np.zeros((2*N,2*N)))

adj_block[N:2*N,0:N] = np.identity(N)

adj_block += adj_block.T

M = mx.MultilayerGraph(list_of_layers=[L1,L2],
                       inter_adjacency_matrix=adj_block)

plt.figure()
plt.imshow(mx.adjacency_matrix(M,weight='weight').todense(),
		  origin='upper',interpolation='nearest',cmap=plt.cm.jet_r)
plt.show()

s = 0.01

classical_closeness, quantum_closeness, degree_arr, classical_closeness_deg,\
 quantum_closeness_deg = closeness_centrality(M, s, procs = 20)

closeness = [x for x in nx.closeness_centrality(M).values()]

np.savetxt("ERBA_ML_RWCC.csv", np.c_[closeness, classical_closeness, quantum_closeness], delimiter = '\t', header = 'closeness \t classical closeness \t quantum closeness')
np.savetxt("ERBA_ML_RWCC_deg.csv", np.c_[degree_arr, classical_closeness_deg, \
            quantum_closeness_deg], delimiter = '\t', header = 'degree \t classical closeness \t quantum closeness')

fig, ax = plt.subplots()
plt.plot(degree_arr, classical_closeness_deg, marker = 'o', ls = 'None', \
     markersize = 4, label=r'quantum')
plt.plot(degree_arr, quantum_closeness_deg, marker = 'x', ls = 'None', \
      markersize = 4, label=r'classical')
 
ax.xaxis.set_minor_locator(MultipleLocator(5))
plt.legend(loc = 4, frameon = False, fontsize = 6)
plt.xlabel(r'degree')
plt.ylabel(r'closeness')
plt.tight_layout()
plt.savefig(r'ERBA_ML_RWCC.png', dpi = 300)

plt.figure()
plt.plot(closeness, quantum_closeness, "o",  markersize = 4, label=r'quantum')
plt.plot(closeness, classical_closeness, "x",  markersize = 4, label=r'classical')
plt.xlabel(r'geodesic closeness')
plt.ylabel(r'random-walk closeness')
plt.legend(loc = 4, frameon = False, fontsize = 6)
plt.tight_layout()
plt.savefig(r'ERBA_ML_RWCC_corr.png', dpi = 300)
plt.show()


