import numpy as np
import networkx as nx
import multinetx as mx
import time
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import scipy as sp
from collections import Counter
from walkerlib.hamiltonians import CHamiltonian, QHamiltonian
from walkerlib.integration import simulate_qc

from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 8,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 183    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

# multilayer network
N = 1000
L1 = mx.generators.erdos_renyi_graph(N, 0.04, seed = 123)
L2 = mx.generators.erdos_renyi_graph(N, 0.06, seed = 355)

adj_block = mx.lil_matrix(np.zeros((2*N,2*N)))

adj_block[N:2*N,0:N] = np.identity(N)

adj_block += adj_block.T

M = mx.MultilayerGraph(list_of_layers=[L1,L2],
                       inter_adjacency_matrix=adj_block)

plt.figure()
plt.imshow(mx.adjacency_matrix(M,weight='weight').todense(),
		  origin='upper',interpolation='nearest',cmap=plt.cm.jet_r)
plt.show()

# quantum Hamiltonian
HQ = QHamiltonian(M)
    
# classical Hamiltonian
HC = CHamiltonian(M)

row = np.array(2*N*[0])
col = np.array([x for x in range(2*N)])
data = np.array(2*N*[1])

last_row = sp.sparse.csr_matrix((data, (row, col)), shape=(1,2*N))

print(last_row.todense())

HC_row = sp.sparse.vstack((HC, last_row))
vec = np.zeros(2*N+1)
vec[-1] = 1

null_space = sp.sparse.linalg.lsqr(HC_row, vec)
null_space = null_space[0]
print(sum(null_space))

nodes = sorted(M.nodes())
    
degrees = M.degree(nodes)
    
degree_sequence = sorted([d for n, d in degrees], reverse=True)
degreeCount = Counter(degree_sequence)

# dictionaries to store betweenness values
degreeProbDictC = {x : 0 for x in degreeCount}

for j in range(len(nodes)):
    degreeProbDictC[M.degree(j)] += null_space[j]

deg, cnt = zip(*degreeCount.items())
classical_observation_prob2 = \
    np.asarray([x for x in degreeProbDictC.values()])/cnt*1e3
    
plt.figure()
plt.plot(deg, classical_observation_prob2)
plt.show()

# time step and simulation time
dt = 0.01
T = 100
N = int(T/dt)

# reset rate
r = 0

# initializing wave function
phim1Q0 = np.ones(len(M))
phim1Q0 /= np.linalg.norm(phim1Q0)    
phiQ0 = phim1Q0-HQ.dot(phim1Q0)*dt

# initializing probability distribution
phiC0 = np.ones(len(M))
phiC0 /= sum(phiC0) 

start_time = time.time()   

# simulate classical and quantum walker
degree_arr, classical_observation_prob, \
            quantum_observation_prob = simulate_qc(M, 
                                                   HC, 
                                                   HQ, 
                                                   phim1Q0, 
                                                   phiQ0, 
                                                   phiC0, 
                                                   N, 
                                                   dt, 
                                                   r, 
                                                   average_threshold = 8000)

print("--- %s seconds runtime ---" % (time.time() - start_time))

np.savetxt("ERER_ML_RWOC.csv", 
           np.c_[degree_arr, classical_observation_prob, quantum_observation_prob], 
           delimiter = '\t', 
           header = 'geodesic betweenness \t classical betweenness \t quantum betweenness')

fig, ax = plt.subplots()
#plt.plot(degree_arr, classical_observation_prob2, ls = '-', linewidth = 2., color = 'k')
plt.plot(degree_arr, quantum_observation_prob, marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
plt.plot(degree_arr, classical_observation_prob, marker = 'x', alpha = 0.8, ls = 'None', \
         markersize = 4, label=r'classical')

plt.text(20+0.02*80, 0.2+0.88*0.8, r"(a)")

ax.xaxis.set_minor_locator(MultipleLocator(5))
plt.xlim([20,100])
plt.ylim([0.2,1.0])
plt.yticks([0.2,0.4,0.6,0.8,1.0])
plt.legend(loc = 4, frameon = False, fontsize = 6)
plt.xlabel(r'degree')
plt.ylabel(r'occupation prob. ($10^{-3}$)')
plt.tight_layout()
plt.savefig(r'ERER_ML_RWOC.png', dpi = 300)