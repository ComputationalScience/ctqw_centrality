import numpy as np
import scipy as sp
import networkx as nx
import multinetx as mx
import time
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

from walkerlib.integration import random_walk_betweenness

from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 10,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 2*183    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

ERER_ML_RWOC = np.loadtxt('ERER_ML_RWOC.csv', dtype = complex)
ERBA_ML_RWOC = np.loadtxt('ERBA_ML_RWOC.csv', dtype = complex)

ERER_ML_RWPR = np.loadtxt('ERER_ML_RWPR.csv', dtype = complex)
ERBA_ML_RWPR = np.loadtxt('ERBA_ML_RWPR.csv', dtype = complex)

fig, ax = plt.subplots(nrows = 2, ncols = 2)

ax[0][0].plot(ERER_ML_RWOC[:,0], ERER_ML_RWOC[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][0].plot(ERER_ML_RWOC[:,0], ERER_ML_RWOC[:,1], marker = 'x', alpha = 0.8, ls = 'None', \
         markersize = 4, label=r'classical')

ax[0][0].text(20+0.02*80, 0.2+0.85*0.8, r"(a)")

ax[0][0].xaxis.set_minor_locator(MultipleLocator(5))
ax[0][0].set_xlim([20,100])
ax[0][0].set_ylim([0.2,1.0])
ax[0][0].set_yticks([0.2,0.4,0.6,0.8,1.0])
ax[0][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[0][0].set_xlabel(r'degree')
ax[0][0].set_ylabel(r'occup. prob. ($10^{-3}$)')

ax[0][1].plot(ERBA_ML_RWOC[:,0], ERBA_ML_RWOC[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[0][1].plot(ERBA_ML_RWOC[:,0], ERBA_ML_RWOC[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')

ax[0][1].text(0.02*60, 0.85*2.5, r"(b)")

ax[0][1].set_xlim([0,60])
ax[0][1].set_ylim([0,2.5])
ax[0][1].set_yticks([0,0.5,1,1.5,2,2.5])
ax[0][1].xaxis.set_minor_locator(MultipleLocator(5))
ax[0][1].legend(loc = 1, frameon = False, fontsize = 6)
ax[0][1].set_xlabel(r'degree')
ax[0][1].set_ylabel(r'occup. prob. ($10^{-3}$)')

ax[1][0].plot(ERER_ML_RWPR[:,0], ERER_ML_RWPR[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][0].plot(ERER_ML_RWPR[:,0], ERER_ML_RWPR[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')

ax[1][0].text(20+0.02*80, 0.2+0.85*0.8, r"(c)")

ax[1][0].xaxis.set_minor_locator(MultipleLocator(5))
ax[1][0].set_xlim([20,100])
ax[1][0].set_ylim([0.2,1.0])
ax[1][0].set_yticks([0.2,0.4,0.6,0.8,1.0])
ax[1][0].legend(loc = 4, frameon = False, fontsize = 6)
ax[1][0].set_xlabel(r'degree')
ax[1][0].set_ylabel(r'occup. prob. ($10^{-3}$)')

ax[1][1].plot(ERBA_ML_RWPR[:,0], ERBA_ML_RWPR[:,2], marker = 'o', ls = 'None', \
         markersize = 4, label=r'quantum')
ax[1][1].plot(ERBA_ML_RWPR[:,0], ERBA_ML_RWPR[:,1], marker = 'x', ls = 'None', \
         markersize = 4, label=r'classical')

ax[1][1].text(0.02*60, 0.85*2.5, r"(d)")

ax[1][1].xaxis.set_minor_locator(MultipleLocator(5))
ax[1][1].set_xlim([0,60])
ax[1][1].set_ylim([0,2.5])
ax[1][1].set_yticks([0,0.5,1,1.5,2,2.5])
ax[1][1].legend(loc = 1, frameon = False, fontsize = 6)
ax[1][1].set_xlabel(r'degree')
ax[1][1].set_ylabel(r'occup. prob. ($10^{-3}$)')

plt.tight_layout()
plt.savefig("oc_synthetic.png", dpi = 480)