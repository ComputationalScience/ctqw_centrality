from numpy import asarray, ones
from scipy.sparse import diags, identity, csr_matrix
from networkx import laplacian_matrix, normalized_laplacian_matrix

def CHamiltonian(G):
    """ 
    Hamiltonian of a classical random walker on a graph G. 
    
    Parameters: 
    G (graph): networkx graph object 
  
    Returns: 
    sparse matrix: Hamiltonian
  
    """
    
    nodes = sorted(G.nodes())
    
    L = laplacian_matrix(G, nodelist=nodes)
    
    degrees = G.degree(nodes)
    degrees_inv = asarray([x[1]**-1.0 for x in degrees])
    
    Dm1 = diags(degrees_inv)
    
    HC = L.dot(Dm1)
    
    return HC

def QHamiltonian(G):
    """ 
    Hamiltonian of a quantum random walker on a graph G. 
    
    Parameters: 
    G (graph): networkx graph object 
  
    Returns: 
    sparse matrix: quantum Hamiltonian
  
    """
    nodes = sorted(G.nodes())
   
    HQ = normalized_laplacian_matrix(G, nodelist=nodes)
    
    return HQ

def pr_CHamiltonian(G,
                    rt):
    """ 
    PageRank Hamiltonian of a classical random walker on a graph G. 
    
    Parameters: 
    G (graph): networkx graph object 
    rt (float): teleportation rate is 1-rt 
    
    Returns: 
    sparse matrix: Hamiltonian
  
    """
    
    nodes = sorted(G.nodes())
    
    L = laplacian_matrix(G, nodelist=nodes)
    
    degrees = G.degree(nodes)
    degrees_inv = asarray([x[1]**-1.0 for x in degrees])
    
    Dm1 = diags(degrees_inv)
    
    HC = L.dot(Dm1)
    
    N = len(nodes)
    
    J = csr_matrix(ones((N,N)))
    
    HC_pr = rt * HC + (1 - rt) * ( identity(N) - 1/N * J )
    
    return HC_pr

def pr_QHamiltonian(G,
                    rt):
    """ 
    PageRank Hamiltonian of a quantum random walker on a graph G. 
    
    Parameters: 
    G (graph): networkx graph object
    rt (float): teleportation rate is 1-rt 
  
    Returns: 
    sparse matrix: quantum Hamiltonian
  
    """
    nodes = sorted(G.nodes())
   
    HQ = normalized_laplacian_matrix(G, nodelist=nodes)
    
    N = len(nodes)

    J = csr_matrix(ones((N,N)))
   
    HQ_pr = rt * HQ + (1 - rt) * ( identity(N) - 1/N * J )

    return HQ_pr

def bc_CHamiltonian(G,
                    k_indices):
    """ 
    PageRank Hamiltonian of a classical random walker on a graph G. 
    
    Parameters: 
    G (graph): networkx graph object 
    k_indices (array): indices of absorbing node k in different layers 
    
    Returns: 
    sparse matrix: Hamiltonian
  
    """
    
    nodes = sorted(G.nodes())
    
    L = laplacian_matrix(G, nodelist=nodes)
    
    degrees = G.degree(nodes)
    degrees_inv = asarray([x[1]**-1.0 for x in degrees])
    
    Dm1 = diags(degrees_inv)
    
    HC = L.dot(Dm1)

    rows, cols = HC.nonzero()
    data = [HC[i,j] if j not in k_indices else 0 for i, j in zip(rows,cols)]
    
    HC_bc = csr_matrix( (data, (rows, cols)), shape=HC.shape )
    
    return HC_bc

def bc_QHamiltonian(G,
                    k_indices):
    """ 
    PageRank Hamiltonian of a quantum random walker on a graph G. 
    
    Parameters: 
    G (graph): networkx graph object
    k_indices (array): indices of absorbing node k in different layers 
  
    Returns: 
    sparse matrix: quantum Hamiltonian
  
    """
    nodes = sorted(G.nodes())
   
    HQ = normalized_laplacian_matrix(G, nodelist=nodes)
    
    rows, cols = HQ.nonzero()
    data = [HQ[i,j] if j not in k_indices else 0 for i, j in zip(rows,cols)]
    
    HQ_bc = csr_matrix( (data, (rows, cols)), shape=HQ.shape )

    return HQ_bc
