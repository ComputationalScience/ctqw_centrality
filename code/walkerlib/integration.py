import numpy as np
from collections import Counter
from walkerlib.hamiltonians import CHamiltonian, QHamiltonian, bc_CHamiltonian, bc_QHamiltonian
from walkerlib.utilities import resolvent_solution, long_time_average
from scipy import sparse
import parmap

def Cintegrate(phi,
               HC, 
               dt):
    """ 
    Explicit Euler integration to simulate a classical random walker.
    
    Parameters: 
    phi (array): prob. distr. at current time
    HC (sparse matrix): classical Hamiltonian
    dt (float): time step
  
    Returns: 
    sparse matrix: prob. distr. at next time step
  
    """

    phip1 = phi-HC.dot(phi)*dt
    
    return phip1

def Qintegrate(phi, 
               phim1, 
               HQ, 
               dt,
               eps = 0):
    """ 
    Integration scheme according to: Askar, Attila, and Ahmet S. Cakmak. 
    "Explicit integration method for the time‐dependent Schrodinger equation for collision problems." 
    The Journal of Chemical Physics 68.6 (1978): 2794-2798. 
    
    Parameters: 
    phi (array): wave function at current time
    phim1 (array): wave function at previous time
    HQ (sparse matrix): quantum Hamiltonian
    dt (float): time step
    eps (float): interpolation parameter (eps = 0 --> quantum, eps = 1 --> classical)

    Returns: 
    sparse matrix: wave function at next time step
  
    """
    phip1 = -2*1.0j*(1-eps)*dt*HQ.dot(phi)+phim1 
    
    return phip1

def simulate_qc(G, 
                HC, 
                HQ, 
                phim1Q0, 
                phiQ0,
                phiC0, 
                N, 
                dt, 
                r, 
                average_threshold = 100,
                degree_average = True):
    """ 
    Run simulation.
    
    Parameters: 
    G (graph): networkx graph object 
    HC (sparse matrix): classical Hamiltonian
    HQ (sparse matrix): quantum Hamiltonian
    phim1Q0 (array): initial wave function (t0)
    phiQ0 (array): initial wave function (t0 + dt)
    phiC0 (array): initial probability vector
    N (int): number of time steps
    dt (float): time step
    r (float): reset rate
    average_threshold (int): number of initial steps before averaging

    Returns: 
    degree array, occupation probabilities
  
    """
    
    assert N > average_threshold, "N should be larger than average_threshold"
    
    phim1Q = np.copy(phim1Q0)            
    phiQ = np.copy(phiQ0)        
    phiC = np.copy(phiC0)        

    nodes = sorted(G.nodes())
    degrees = G.degree(nodes)

    degree_sequence = sorted([d for n, d in degrees], reverse=True)
    degreeCount = Counter(degree_sequence)
    
    # dictionaries to store probabilities of walkers to occupy nodes
    # with certain degrees
    degreeProbDictQ = {x : 0 for x in degreeCount}
    degreeProbDictC = {x : 0 for x in degreeCount}
    
    number_samples = 0
    
    classical_cont = np.zeros_like(phiC)
    quantum_cont = np.zeros_like(phiQ, dtype = complex)
    
    for i in range(N):
        
        eps = np.random.rand()
        
        # reset
        if eps < r*dt:
            
            # reset quantum walker
            phim1Q = np.copy(phim1Q0)  
            phiQ = np.copy(phiQ0)     
            
            # reset classical walker
            phiC = np.copy(phiC0) 
    
        # walker evolution according to Hamiltonian dynamics
        else:
            
            # evolution of quantum walker
            phip1Q = Qintegrate(phiQ, phim1Q, HQ, dt)
            phim1Q = np.copy(phiQ)
            phiQ = np.copy(phip1Q)      
                        
            # evolution of classical walker
            phip1C = Cintegrate(phiC, HC, dt)
            phiC = np.copy(phip1C)  
            
            if i > average_threshold:
                
                number_samples += 1
                
                quantum_cont += phiQ*np.conj(phiQ)
                classical_cont += phiC
    
                for j in range(len(nodes)):
                    degreeProbDictQ[G.degree(j)] += abs(phiQ[j])**2  
                    degreeProbDictC[G.degree(j)] += phiC[j]
    
    for x in degreeCount:
        degreeProbDictQ[x] *= 1./number_samples
        degreeProbDictC[x] *= 1./number_samples
    
    quantum_cont *= 1./number_samples
    classical_cont *= 1./number_samples
    
    deg, cnt = zip(*degreeCount.items())

    cnt = np.asarray(cnt)

    classical_observation_prob = \
    np.asarray([x for x in degreeProbDictC.values()])/cnt*1e3
    
    quantum_observation_prob = \
    np.asarray([x for x in degreeProbDictQ.values()])/cnt*1e3
    
    if degree_average:
        return deg, classical_observation_prob, quantum_observation_prob
    
    else:
        return classical_cont, quantum_cont

def multiprocessing_betweenness(k, 
                                s, 
                                phiC0, 
                                phim1Q0, 
                                N):
    """ 
    Compute the probability that a random walker
    (with absorbing node k )that starts at (o,sigma)
    passes through (j,beta) at time [t,t+dt).
    
    Parameters: 
    k (integer): absorbing node-layer
    phiC0 (array): initial probability vector
    phim1Q0 (array): initial wave function
    N (integer): number of node-layers 

    Returns: 
    tau_cl, tau_q: classical and quantum betweenness
  
    """
    
    HC_bc = sparse.load_npz("walkerlib/hamiltonians/HC_bc_%d.npz"%k)

    HQ_bc = sparse.load_npz("walkerlib/hamiltonians/HQ_bc_%d.npz"%k)
    
    tau_k_cl = resolvent_solution(HC_bc, s, phiC0)
    
    tau_k_q = long_time_average(HQ_bc, s, phim1Q0)
    
    return tau_k_cl, tau_k_q
    
def random_walk_betweenness(M, 
                            s, 
                            procs, 
                            closeness = False):
    """ 
    Compute the probability that a random walker
    (with absorbing node k ) that starts at (o,sigma)
    passes through (j,beta) at time [t,t+dt).
    
    Parameters: 
    M (mx graph): multilayer graph
    s (float): resolvent parameter
    procs (integer): number of parallel processes
    closeness (Boolean): return betweenness arrays for closeness calculations

    Returns: 
    degree array, occupation probabilities
  
    """
    
    nodes = sorted(M.nodes())

    tau_q = 0
    tau_cl = 0

    # initializing prob. vector
    phiC0 = np.ones(len(M))
    phiC0 /= sum(phiC0) 

    # initializing wave function
    phim1Q0 = np.ones(len(M))
    phim1Q0 /= np.linalg.norm(phim1Q0)    
    
    # generating and saving Hamiltonians
    for k in nodes:
        HC_bc = bc_CHamiltonian(M, [k])
        HQ_bc = bc_QHamiltonian(M, [k])
        
        sparse.save_npz("walkerlib/hamiltonians/HC_bc_%d.npz"%k, HC_bc)
        sparse.save_npz("walkerlib/hamiltonians/HQ_bc_%d.npz"%k, HQ_bc)
    
    # parallelize betweenness calculations
    tau_multi = parmap.starmap(multiprocessing_betweenness, 
                               [(k, s, phiC0, phim1Q0, len(nodes)) for k in nodes], 
                               pm_pbar=True, pm_processes=procs)
    
    tau_cl = np.sum([x[0] for x in tau_multi], axis = 0)
    tau_q = np.sum([x[1] for x in tau_multi], axis = 0)
    
    tau_cl /= len(M) * (len(M) - 1)
    tau_q /= len(M) * (len(M) - 1) 
    
    tau_cl_closeness = np.sum([x[0] for x in tau_multi], axis = 1)
    tau_q_closeness = np.sum([x[1] for x in tau_multi], axis = 1)
        
    degrees = M.degree(nodes)
    
    degree_sequence = sorted([d for n, d in degrees], reverse=True)
    degreeCount = Counter(degree_sequence)

    # dictionaries to store betweenness values
    degreeProbDictQ = {x : 0 for x in degreeCount}
    degreeProbDictC = {x : 0 for x in degreeCount}
    
    for j in range(len(nodes)):
        degreeProbDictQ[M.degree(j)] += tau_q[j]  
        degreeProbDictC[M.degree(j)] += tau_cl[j]
    
    deg, cnt = zip(*degreeCount.items())

    cnt = np.asarray(cnt)

    classical_tau = np.asarray([x for x in degreeProbDictC.values()])/cnt
    
    quantum_tau = np.asarray([x for x in degreeProbDictQ.values()])/cnt

    if closeness:
        return tau_cl_closeness, tau_q_closeness 

    else:
        return tau_cl, tau_q, deg, classical_tau, quantum_tau

def closeness_centrality(M, 
                         s, 
                         procs):
    
    """ 
    Compute classical and quantum closeness centralities for node-layers 
    of the multilayer network M.
    
    Parameters: 
    M (mx graph): multilayer graph
    s (float): resolvent parameter
    procs (integer): number of parallel processes
    
    Returns: 
    closeness_cl, closeness_q, deg, closeness_cl_deg, closeness_q_deg: 
    classical and quantum closeness centralities (for each node-layer) and 
    aggregated per degree
  
    """   
    
    # quantum Hamiltonian
    HQ = QHamiltonian(M)
        
    # classical Hamiltonian
    HC = CHamiltonian(M)
    
    # time step and simulation time
    dt = 0.01
    T = 100
    N = int(T/dt)
    
    # reset rate
    r = 0
    
    # initializing wave function
    phim1Q0 = np.ones(len(M))
    phim1Q0 /= np.linalg.norm(phim1Q0)    
    phiQ0 = phim1Q0-HQ.dot(phim1Q0)*dt
    
    # initializing probability distribution
    phiC0 = np.ones(len(M))
    phiC0 /= sum(phiC0) 
        
    # simulate classical and quantum walker
    classical_cont, quantum_cont = simulate_qc(M, 
                                               HC, 
                                               HQ, 
                                               phim1Q0, 
                                               phiQ0, 
                                               phiC0, 
                                               N, 
                                               dt, 
                                               r, 
                                               average_threshold = 8000,
                                               degree_average = False)

    tau_cl_closeness, tau_q_closeness = random_walk_betweenness(M, 
                                                                s, 
                                                                procs, 
                                                                closeness = True)
    
    h_cl = classical_cont**-1/len(phiC0) + tau_cl_closeness/(len(phiC0)-1)
    
    h_q = quantum_cont**-1/len(phiQ0) + tau_q_closeness/(len(phiQ0)-1)
    
    closeness_cl = h_cl**-1
    
    closeness_q = h_q**-1
    
    nodes = sorted(M.nodes())

    degrees = M.degree(nodes)
    
    degree_sequence = sorted([d for n, d in degrees], reverse=True)
    degreeCount = Counter(degree_sequence)

    # dictionaries to store betweenness values
    degreeProbDictQ = {x : 0 for x in degreeCount}
    degreeProbDictC = {x : 0 for x in degreeCount}
    
    for j in range(len(nodes)):
        degreeProbDictQ[M.degree(j)] += closeness_q[j]  
        degreeProbDictC[M.degree(j)] += closeness_cl[j]
    
    deg, cnt = zip(*degreeCount.items())

    cnt = np.asarray(cnt)

    closeness_cl_deg = np.asarray([x for x in degreeProbDictC.values()])/cnt
    
    closeness_q_deg = np.asarray([x for x in degreeProbDictQ.values()])/cnt

    return closeness_cl, closeness_q, deg, closeness_cl_deg, closeness_q_deg
    
    
    
    
    
    
    
    
