import numpy as np
import multinetx as mx
from scipy.sparse.linalg import expm, spsolve
from scipy.sparse import identity
from numpy import linspace, trapz
from scipy.special import binom
from itertools import combinations 

def load_multiplex(path):
    """ 
    Load London tube and Lazega law firm 
    (and maybe? other https://networks.skewed.de/net/) multiplex networks. 
    
    Parameters: 
    path (string): file path of edge list that 
		   contains source, target, layer information
    
    Returns: 
    M: multilayer network
  
    """

    edge_list = np.loadtxt(path, delimiter = ',')
    source, target, layer = edge_list[:,0], edge_list[:,1], edge_list[:,3]

    layers = []
    
    layer_num = np.unique(layer)
    
    N = int(max([max(source),max(target)]))+1

    for l in layer_num:
        s = source[layer == l]
        t = target[layer == l]
        
        edges = [(i,j) for (i,j) in zip(s,t)]
        
        L = mx.Graph()
        L.add_nodes_from([x for x in range(N)])
        L.add_edges_from(edges)
        
        layers.append(L)

    adj_dim = int(binom(len(layer_num), 2))
    
    adj_block = mx.lil_matrix(np.zeros((adj_dim*N,adj_dim*N)))
    
    comb = combinations(layer_num, 2)
    
    for c in comb:    
        i = int(c[0]-1)
        j = int(c[1]-1)
        
        adj_block[i*N:(i+1)*N, j*N:(j+1)*N] = np.identity(N)
        
    adj_block += adj_block.T

    M = mx.MultilayerGraph(list_of_layers=layers,
                       inter_adjacency_matrix=adj_block)

    return M

def compute_exp_matrix_integration(A, 
                                   T, 
                                   nbins=200):
    """ 
    Compute matrix exponential numerically.
    
    Parameters: 
    A (array): matrix that appears in exp(-A*t)
    T (float): final integration interval [0,T] time 
    nbins (integer): discretization of [0,T]
    
    Returns: 
    result (float): numerical integration result of exp(-A*t) on [0,T]
  
    """
    
    f = lambda x: expm(-A*x)
    
    xv = linspace(0, T, nbins)
    
    result = [f(x) for x in xv]
    
    result = trapz(result, xv)
    
    return result


def resolvent_solution(A, 
                       s, 
                       x):
    """ 
    Solve a resolvent equation numerically.
    
    Parameters: 
    A (array): matrix that appears in exp(-A*t)
    s (float): resolvent parameter
    x (array): r.h.s. vector 
    
    Returns: 
    result (array): solution vector (corresponding to betweenness centrality
                    in our calculations)
  
    """
    
    return spsolve(s*identity(A.get_shape()[0])+A, x)


def long_time_average(HQ, 
                      r, 
                      phim1Q0):
    """ 
    Stationary occupation probability of a quantum random walker on a graph G. 
    
    Parameters: 
    HQ (sparse matrix): quantum Hamiltonian
    phim1Q0 (array): initial wave function
    r (float): reset rate

    Returns: 
    arrays: degrees, stationary occupation probability distribution
  
    """
    
    eigenvalues, eigenvectors = np.linalg.eigh(HQ.todense())

    stationary_distr_q = np.zeros(phim1Q0.size, dtype = complex)
    
    for k in range(eigenvalues.size):

        for kp in range(len(eigenvalues)):
            
            v1 = np.ravel(eigenvectors[:,k])
            v2 = np.ravel(eigenvectors[:,kp])
                        
            prod1 = np.dot(phim1Q0, np.conj(v1))
            prod2 = np.dot(np.conj(phim1Q0), v2)
            
            stationary_distr_q += \
            v1*v2*1/(r+1.0j*(eigenvalues[k]-eigenvalues[kp]))*prod1*prod2
           
    return stationary_distr_q
